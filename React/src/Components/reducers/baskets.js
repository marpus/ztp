import { ADD_BASKET, REMOVE_BASKET } from '../../actions/types'

const initialState = {
    data: []
}


export default (state = initialState, action) => {
    const newData = [ ...state.data ];

    switch(action.type){
        case ADD_BASKET:
            newData.push(action.payload);

            return {
                ...state,
                data: newData
            }
        case REMOVE_BASKET:
            const key = action.payload;

            newData.splice(key, 1);

            return {
                ...state,
                data: newData
            }
        default:
            return state;
    }
}