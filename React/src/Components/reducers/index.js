import {combineReducers} from 'redux';
import basketReducer from './basketReducer'
import baskets from './baskets';

export default combineReducers({
    basketState: basketReducer,
    baskets
});