import { ADD_BASKET, ADD_PRODUCT_BASKET, ADD_PRODUCT_BASKET_TO_PANEL, REMOVE_BASKET } from './types'

export const addBasket2  = basket => {
    return(dispatch)=>{
      dispatch({
          type: ADD_BASKET,
          payload: basket
          
      });
    }
}

export const removeBasket2 = key => {
  return(dispatch) => {
    dispatch({
      type: REMOVE_BASKET,
      payload: key
    })
  }
}

export const addBasket  = (productName, quantity)=>{
    return(dispatch)=>{
      console.log("adding to Basket");
      console.log("Product: ", productName);
      dispatch({
          type: ADD_PRODUCT_BASKET,
          payload: productName, quantity
          
      });
    }
}


export const addBasketToPanel  = (productName)=>{
  return(dispatch)=>{
    console.log("adding to Basket Panel");
    console.log("Product: ", productName);
    dispatch({
        type: ADD_PRODUCT_BASKET_TO_PANEL,
        payload: productName
        
    });
  }
}