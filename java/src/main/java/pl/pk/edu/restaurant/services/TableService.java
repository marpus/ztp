package pl.pk.edu.restaurant.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.pk.edu.restaurant.beans.STable;
import pl.pk.edu.restaurant.repository.TableRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class TableService {
    @Autowired
    private TableRepository tableRepository;

    public List<STable> getAll() {
        List<STable> tables = new ArrayList<>();
        tableRepository.findAll().forEach(tables::add);
        return tables;
    }

    public Optional<STable> getById(Integer dishId) { return tableRepository.findById(dishId); }

    public STable add(STable table){
        return tableRepository.save(table);
    }

    public void remove(STable table) { tableRepository.delete(table); }

    public void remove(Integer id) { tableRepository.deleteById(id); }

    public STable update(STable table) { return tableRepository.save(table); }
}
