package pl.pk.edu.restaurant.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.pk.edu.restaurant.beans.DishCategory;
import pl.pk.edu.restaurant.repository.DishCategoryRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class DishCategoryService {
    @Autowired
    private DishCategoryRepository dishCategoryRepository;

    public List<DishCategory> getAll() {
        List<DishCategory> dishes = new ArrayList<>();
        dishCategoryRepository.findAll().forEach(dishes::add);
        return dishes;
    }

    public Optional<DishCategory> getById(Integer dishId) { return dishCategoryRepository.findById(dishId); }

    public DishCategory add(DishCategory dish){
        return dishCategoryRepository.save(dish);
    }

    public void remove(DishCategory dish) { dishCategoryRepository.delete(dish); }

    public void remove(Integer dishId) { dishCategoryRepository.deleteById(dishId); }

    public DishCategory update(DishCategory dish) { return dishCategoryRepository.save(dish); }
}
