package pl.pk.edu.restaurant.constants;

public class SecurityConstants {
    public static final String SECRET = "PKRestaurantSecurityConst";
    public static final long EXPIRATION_TIME = 86_400_000; // 1 days
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
    public static final String AUTH_TOKEN_URL = "/api/auth/token";
}
