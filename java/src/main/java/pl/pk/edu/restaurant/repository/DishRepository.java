package pl.pk.edu.restaurant.repository;

import org.springframework.data.repository.CrudRepository;
import pl.pk.edu.restaurant.beans.Dish;

public interface DishRepository extends CrudRepository<Dish, Integer> {

}
