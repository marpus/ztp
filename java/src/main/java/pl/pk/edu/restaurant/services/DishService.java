package pl.pk.edu.restaurant.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.pk.edu.restaurant.beans.Dish;
import pl.pk.edu.restaurant.repository.DishRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class DishService {
    @Autowired
    private DishRepository dishRepository;

    public List<Dish> getAll() {
        List<Dish> dishes = new ArrayList<>();
        dishRepository.findAll().forEach(dishes::add);
        return dishes;
    }

    public Optional<Dish> getById(Integer dishId) { return dishRepository.findById(dishId); }

    public Dish add(Dish dish){
        return dishRepository.save(dish);
    }

    public void remove(Dish dish) { dishRepository.delete(dish); }

    public void remove(Integer dishId) { dishRepository.deleteById(dishId); }

    public Dish update(Dish dish) { return dishRepository.save(dish); }
}
