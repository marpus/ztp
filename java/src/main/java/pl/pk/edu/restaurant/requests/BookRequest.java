package pl.pk.edu.restaurant.requests;

import javax.validation.constraints.NotBlank;

public class BookRequest {
    @NotBlank(message = "Title have to be filled up")
    private String title;
    private String author;
    private String year;

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public String getYear() {
        return year;
    }
}
