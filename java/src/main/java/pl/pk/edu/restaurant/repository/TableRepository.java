package pl.pk.edu.restaurant.repository;

import org.springframework.data.repository.CrudRepository;
import pl.pk.edu.restaurant.beans.STable;

public interface TableRepository extends CrudRepository<STable, Integer> {

}
