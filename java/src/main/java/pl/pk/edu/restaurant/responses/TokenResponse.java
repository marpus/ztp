package pl.pk.edu.restaurant.responses;

public class TokenResponse {
    public String value;

    public TokenResponse(String token) {
        this.value = token;
    }
}
