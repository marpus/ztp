package pl.pk.edu.restaurant.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import pl.pk.edu.restaurant.beans.Role;
import pl.pk.edu.restaurant.beans.ApplicationUser;
import pl.pk.edu.restaurant.services.UserService;

import java.security.Principal;

@Controller
@RequestMapping(path="/user")
public class UserController {
    @Autowired
    private UserService userService;

    @GetMapping()
    public @ResponseBody Iterable<ApplicationUser> getAllUsers(Authentication authentication, Principal principal) {
        return userService.getAll();
    }

    @PostMapping()
    public @ResponseBody ApplicationUser addNewUser () {
        ApplicationUser n = new ApplicationUser("UserName","FirstName","LastName","Email", Role.Manager);
        return userService.add(n);
    }
}
