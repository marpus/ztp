package pl.pk.edu.restaurant.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.pk.edu.restaurant.beans.ApplicationUser;
import pl.pk.edu.restaurant.repository.UserRepository;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public List<ApplicationUser> getAll() {
        List<ApplicationUser> users = new ArrayList<>();
        userRepository.findAll().forEach(users::add);
        return users;
    }

    public ApplicationUser add(ApplicationUser user){
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        return userRepository.save(user);
    }
}
